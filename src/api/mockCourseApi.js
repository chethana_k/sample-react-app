import delay from './delay';

// This file mocks a web API by working with the hard-coded data below.
// It uses setTimeout to simulate the delay of an AJAX call.
// All calls return promises.
const courses = [
  {
    id: "jira-essentials-ondemand",
    title: "Jira Essentials Ondemand",
    gotoHref: "https://training.atlassian.com/elearning/jira-essentials-ondemand",
    authorId: "Jira Team",
    cost: "$300",
    category: "Jira"
  },
  {
    id: "jira-servicedesk-administrator-certification-prep-on-demand",
    title: "Jira Servicedesk Administrator Certification Prep on demand",
    gotoHref: "https://training.atlassian.com/elearning/jira-servicedesk-administrator-certification-prep-on-demand",
    authorId: "Service Desk Team",
    cost: "$150",
    category: "Jira Service Desk"
  },
  {
    id: "learn-jira-basics-cloud",
    title: "Learn Jira Basics Cloud",
    gotoHref: "https://training.atlassian.com/course/learn-jira-basics-cloud",
    authorId: "Jira Cloud Team",
    cost: "Free",
    category: "Jira Cloud"
  },
  {
    id: "confluence-content-management",
    title: "Confluence content management",
    gotoHref: "https://training.atlassian.com/elearning/confluence-content-management",
    authorId: "Confluence Content Team",
    cost: "Free",
    category: "Confluence"
  },
  {
    id: "project-administration-in-jira-server",
    title: "Project Administration in Jira Server",
    gotoHref: "https://training.atlassian.com/course/project-administration-in-jira-server",
    authorId: "Jira Server Team",
    cost: "$300",
    category: "Jira Server"
  }
];

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
}

//This would be performed on the server in a real app. Just stubbing in.
const generateId = (course) => {
  return replaceAll(course.title, ' ', '-');
};

class CourseApi {
  static getAllCourses() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.assign([], courses));
      }, delay);
    });
  }

  static saveCourse(course) {
    course = Object.assign({}, course); // to avoid manipulating object passed in.
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        // Simulate server-side validation
        const minCourseTitleLength = 1;
        if (course.title.length < minCourseTitleLength) {
          reject(`Title must be at least ${minCourseTitleLength} characters.`);
        }

        if (course.id) {
          const existingCourseIndex = courses.findIndex(a => a.id == course.id);
          courses.splice(existingCourseIndex, 1, course);
        } else {
          //Just simulating creation here.
          //The server would generate ids and gotoHref's for new courses in a real app.
          //Cloning so copy returned is passed by value rather than by reference.
          course.id = generateId(course);
          course.gotoHref = `https://training.atlassian.com/course/${course.id}`;
          courses.push(course);
        }

        resolve(course);
      }, delay);
    });
  }

  static deleteCourse(courseId) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const indexOfCourseToDelete = courses.findIndex(course => {
          course.id == courseId;
        });
        courses.splice(indexOfCourseToDelete, 1);
        resolve();
      }, delay);
    });
  }
}

export default CourseApi;
