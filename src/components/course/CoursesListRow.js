import React, {PropTypes} from 'react';
import {Link} from 'react-router';

const CoursesListRow = ({course}) => {
  return (
    <tr>
      <td><a href={course.gotoHref} target="_blank">Go to</a></td>
      <td><Link to={'/course/' + course.id}>{course.title}</Link></td>
      <td>{course.authorId}</td>
      <td>{course.category}</td>
      <td>{course.cost}</td>
    </tr>
  );
};

CoursesListRow.propTypes = {
  course: PropTypes.object.isRequired
};

export default CoursesListRow;
